# Databricks notebook source
# !pip install openpyxl
# !pip install fsspec
# !pip install gcsfs
# !pip install xlsxwriter
# !pip install bigquery
# !pip install pandas-gbq

# COMMAND ----------

import pandas as pd
import numpy as np
import io
from pyspark.sql.functions import *
from pyspark.sql.types import DoubleType, IntegerType, StringType, DateType, StructType, StructField
from pyspark.sql import Window
import datetime
from datetime import timedelta
import unicodedata
import numpy as np
import scipy.stats as stats
import pytz

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

from google.cloud import bigquery as bq
from google.cloud import storage
client = bq.Client()

# pd.set_option('display.max_columns', None)

# import pydata_google_auth
# credentials = pydata_google_auth.get_user_credentials( ['https://www.googleapis.com/auth/bigquery'] )
# %reload_ext google.cloud.bigquery
# from google.cloud.bigquery import magics
# magics.context.credentials = credentials
# from google.cloud import bigquery as bq

# client = bq.Client(project="seara-analytics-dev", credentials=credentials)
# client = bq.Client()

# Cria método para mostrar o shape do df de maneira rápida
import pyspark
def sparkShape(dataFrame):
    return (dataFrame.count(), len(dataFrame.columns))
pyspark.sql.dataframe.DataFrame.shape = sparkShape

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

# COMMAND ----------

dataset = bq.dataset.Dataset("seara-auditoria-cfo-nonprod.capex_cashflow")

# COMMAND ----------

# MAGIC %md
# MAGIC ### Query valor realizado dos investimentos

# COMMAND ----------

# RATEIO

comentario = """
RATEIO_ITEM_NFI_SNAC AS ( 
  SELECT
  CD_EMPRESA,
  CD_FILIAL,
  CD_BASE_FORNECEDOR,
  CD_ESTAB_FORNECEDOR,
  CD_DOCUMENTO,
  CD_SERIE_DOCUMENTO,
  CD_CENTRO_CUSTO,
  CD_CONTA_CONTABIL,
  SUM(VL_RATEIO_AJUSTADO) AS VL_RATEIO_AJUSTADO
FROM
  `seara-analytics-prod.stg_erp_seara.rateio_item_nfi_snac`
GROUP BY 
  CD_EMPRESA,
  CD_FILIAL,
  CD_BASE_FORNECEDOR,
  CD_ESTAB_FORNECEDOR,
  CD_DOCUMENTO,
  CD_SERIE_DOCUMENTO,
  CD_CENTRO_CUSTO,
  CD_CONTA_CONTABIL
 )
 
 LEFT JOIN
 
 LEFT JOIN 
  RATEIO_ITEM_NFI_SNAC 
ON
  RATEIO_ITEM_NFI_SNAC.CD_CENTRO_CUSTO = CCU_CTA_CTV_ITEM_INV.CD_CENTRO_CUSTO_DEBITO
  AND RATEIO_ITEM_NFI_SNAC.CD_CONTA_CONTABIL = CCU_CTA_CTV_ITEM_INV.CD_CONTA_CTB_DEBITO
  AND RATEIO_ITEM_NFI_SNAC.CD_EMPRESA = OBRIGACAO_PAGAR.CD_EMPRESA_RECEPTORA
  AND RATEIO_ITEM_NFI_SNAC.CD_FILIAL = OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA
  AND RATEIO_ITEM_NFI_SNAC.CD_BASE_FORNECEDOR = OBRIGACAO_PAGAR.CD_BASE_CREDOR
  AND RATEIO_ITEM_NFI_SNAC.CD_ESTAB_FORNECEDOR = OBRIGACAO_PAGAR.CD_ESTAB_CREDOR
  AND CONCAT(RATEIO_ITEM_NFI_SNAC.CD_DOCUMENTO,'/',RATEIO_ITEM_NFI_SNAC.CD_SERIE_DOCUMENTO) = OBRIGACAO_PAGAR.CD_DOCUMENTO_ORIGEM
 
 """

# COMMAND ----------

query = f"""
WITH ITEM_DOCUMENTO_RECEBIMENTO AS (  
  SELECT
    CD_EMPRESA_INV,
    CD_FILIAL_INV,
    CD_INVESTIMENTO, 
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO,
    STRING_AGG(CAST(ITEM_DOC.CD_ITEM AS STRING), ' / ') AS CD_ITEM,
    STRING_AGG(ITEM.NM_ITEM, ' / ') AS NM_ITEM,
    CD_ORDEM_FORNECIMENTO,
    SUM(VL_CALCULO_INV) AS VL_CALCULO_INV,
    SUM(VL_CALCULO_OFO) AS VL_CALCULO_OFO
  FROM
    `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` ITEM_DOC
  LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item` ITEM
  ON
    ITEM_DOC.CD_ITEM = ITEM.CD_ITEM
  GROUP BY
    CD_ORDEM_FORNECIMENTO,
    CD_EMPRESA_INV,
    CD_FILIAL_INV,
    CD_INVESTIMENTO,
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO
)

SELECT
  CONCAT(ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV,'-',ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV,'-',ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO) AS CHAVE,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO,
  INVESTIMENTO.ID_SITUACAO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ITEM,
  ITEM_DOCUMENTO_RECEBIMENTO.NM_ITEM,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ORDEM_FORNECIMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.VL_CALCULO_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.VL_CALCULO_OFO,
  PARSE_DATE('%d/%m/%Y', LEFT(DOCUMENTO_RECEBIMENTO.DT_INTEGRACAO,10)) AS DT_INTEGRACAO,
FROM
  ITEM_DOCUMENTO_RECEBIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.investimento` INVESTIMENTO 
ON
  ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO = INVESTIMENTO.CD_INVESTIMENTO
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV = INVESTIMENTO.CD_EMPRESA 
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV = INVESTIMENTO.CD_FILIAL
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.documento_recebimento` DOCUMENTO_RECEBIMENTO
ON
  DOCUMENTO_RECEBIMENTO.CD_EMPRESA_PGT = ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV
  AND DOCUMENTO_RECEBIMENTO.CD_FILIAL_PGT = ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV
  AND DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR
  AND DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR
WHERE
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_PREVISAO_INICIO,10)) >= '2019-01-01'
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO = 6282
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV = 30
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV = 228
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO = 44923
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO = '1'
"""

# df_pagamentos = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_documentos = client.query(query).to_dataframe()

# COMMAND ----------

for coluna in ['VL_CALCULO_INV', 'VL_CALCULO_OFO']:

  df_documentos[coluna] = df_documentos[coluna].astype(float)

# COMMAND ----------

table_id = "capex_documentos"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_documentos, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# df_documentos.to_excel('gs://capex_cashflow/documentos_investimentos.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Query fluxo de pagamentos dos investimentos

# COMMAND ----------

query = f"""
WITH ITEM_DOCUMENTO_RECEBIMENTO AS (  
  SELECT
    CD_EMPRESA_INV,
    CD_FILIAL_INV,
    CD_INVESTIMENTO, 
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO,
    STRING_AGG(CAST(ITEM_DOC.CD_ITEM AS STRING), ' / ') AS CD_ITEM,
    STRING_AGG(ITEM.NM_ITEM, ' / ') AS NM_ITEM,
    SUM(VL_CALCULO_INV) AS VL_CALCULO_INV,
    SUM(VL_CALCULO_OFO) AS VL_CALCULO_OFO
  FROM
    `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` ITEM_DOC
  LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.item` ITEM
  ON
    ITEM_DOC.CD_ITEM = ITEM.CD_ITEM
  GROUP BY
    CD_EMPRESA_INV,
    CD_FILIAL_INV,
    CD_INVESTIMENTO,
    CD_BASE_FORNECEDOR,
    CD_ESTAB_FORNECEDOR,
    CD_DOCUMENTO,
    CD_SERIE_DOCUMENTO
)

SELECT
  CONCAT(ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV,'-',ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV,'-',ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO) AS CHAVE,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO,
  INVESTIMENTO.ID_SITUACAO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_ITEM,
  ITEM_DOCUMENTO_RECEBIMENTO.NM_ITEM,
  TIPO_OBRIGACAO_PAGAR.DS_TIPO_OBRIGACAO,
  SITUACAO_OBRIGACAO_PAGAR.DS_SITUACAO_OBRIGACAO,
  SITUACAO_OBRIGACAO_PAGAR.ID_PERMITE_ALTERAR_OBRIGACAO,	
  OBRIGACAO_PAGAR.NR_DOCUMENTO,
  OBRIGACAO_PAGAR.NR_SEQUENCIA,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_VENCIMENTO,10)) AS DT_VENCIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_PREVISAO_PAGAMENTO,10)) AS DT_PREVISAO_PAGAMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_PAGAMENTO,10)) AS DT_PAGAMENTO,
  OBRIGACAO_PAGAR.VL_DOCUMENTO,
  OBRIGACAO_PAGAR.VL_SALDO,
  OBRIGACAO_PAGAR.VL_BRUTO,	
  OBRIGACAO_PAGAR.VL_PAGAMENTO,
  -- ITEM_DOCUMENTO_RECEBIMENTO.VL_CALCULO_INV,
  -- SUM(OBRIGACAO_PAGAR.VL_DOCUMENTO) OVER( 
  --   PARTITION BY 
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV, 
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV, 
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO,
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR,
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR,
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
  --     ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  --  ) AS VL_TOTAL_DOCUMENTO,
  VL_PAGAMENTO*SAFE_DIVIDE(
    ITEM_DOCUMENTO_RECEBIMENTO.VL_CALCULO_INV,
    SUM(OBRIGACAO_PAGAR.VL_DOCUMENTO) OVER( 
    PARTITION BY 
      ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV, 
      ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV, 
      ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO,
      ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR,
      ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR,
      ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
      ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
   )
   ) AS VL_PAGAMENTO_AJUSTADO,
  PARSE_DATE('%d/%m/%Y', LEFT(DOCUMENTO_RECEBIMENTO.DT_INTEGRACAO,10)) AS DT_INTEGRACAO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_EMISSAO,10)) AS DT_EMISSAO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_INCLUSAO,10)) AS DT_INCLUSAO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_AUTORIZACAO_PAGAMENTO,10)) AS DT_AUTORIZACAO_PAGAMENTO,
  CCU_CTA_CTV_ITEM_INV.CD_CENTRO_CUSTO_DEBITO,	
  CENTRO_CUSTO.NM_CENTRO_CUSTO,
  CCU_CTA_CTV_ITEM_INV.CD_CONTA_CTB_DEBITO,
  OBRIGACAO_PAGAR.CD_EMPRESA_RECEPTORA,
  OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA,
  OBRIGACAO_PAGAR.CD_BANCO_CONTA_CREDITO,
  OBRIGACAO_PAGAR.CD_AGENCIA_CONTA_CORRENTE_CRE,
  OBRIGACAO_PAGAR.CD_DIGITO_AGENCIA_CREDITO,
  OBRIGACAO_PAGAR.CD_CONTA_CORRENTE_CREDITO,
  OBRIGACAO_PAGAR.CD_DIGITO_CONTA_CORRENTE_CRE,
FROM
  ITEM_DOCUMENTO_RECEBIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.documento_recebimento` DOCUMENTO_RECEBIMENTO
ON
  DOCUMENTO_RECEBIMENTO.CD_EMPRESA_PGT = ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV
  AND DOCUMENTO_RECEBIMENTO.CD_FILIAL_PGT = ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV
  AND DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  AND DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR
  AND DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR
LEFT JOIN
  (
    SELECT 
      *,
    ROW_NUMBER() OVER(
      PARTITION BY 
        CD_DOCUMENTO_ORIGEM, 
        CD_FILIAL_RECEPTORA, 
        CD_BASE_CREDOR, 
        CD_ESTAB_CREDOR 
      ORDER BY 
        PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_ATUALIZACAO) DESC
    ) AS rn 
    FROM 
      `seara-analytics-prod.stg_erp_seara.obrigacao_pagar`

  ) AS OBRIGACAO_PAGAR  
ON
--   OBRIGACAO_PAGAR.rn = 1 AND
  CONCAT(ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO, '/',ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO) = OBRIGACAO_PAGAR.CD_DOCUMENTO_ORIGEM
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR = OBRIGACAO_PAGAR.CD_BASE_CREDOR
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = OBRIGACAO_PAGAR.CD_ESTAB_CREDOR
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV = OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV = OBRIGACAO_PAGAR.CD_EMPRESA_RECEPTORA
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.investimento` INVESTIMENTO 
ON
  ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO = INVESTIMENTO.CD_INVESTIMENTO
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV = INVESTIMENTO.CD_EMPRESA 
  AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV = INVESTIMENTO.CD_FILIAL
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.situacao_obrigacao_pagar` SITUACAO_OBRIGACAO_PAGAR  
ON
  OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO = SITUACAO_OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.tipo_obrigacao_pagar` TIPO_OBRIGACAO_PAGAR  
ON
  OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO = TIPO_OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.ccu_cta_ctb_item_inv` CCU_CTA_CTV_ITEM_INV
ON
  CCU_CTA_CTV_ITEM_INV.CD_EMPRESA = ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV
  AND CCU_CTA_CTV_ITEM_INV.CD_FILIAL = ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV
  AND CCU_CTA_CTV_ITEM_INV.CD_INVESTIMENTO = ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.centro_custo` CENTRO_CUSTO 
ON
  CENTRO_CUSTO.CD_CENTRO_CUSTO = CCU_CTA_CTV_ITEM_INV.CD_CENTRO_CUSTO_DEBITO
WHERE
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_PREVISAO_INICIO,10)) >= '2019-01-01'
  AND ITEM_DOCUMENTO_RECEBIMENTO.VL_CALCULO_INV > 0
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO = 491
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV = 30
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV = 351
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO = 4612
  -- AND ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO = '1'
ORDER BY
  ITEM_DOCUMENTO_RECEBIMENTO.CD_EMPRESA_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_FILIAL_INV,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_INVESTIMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO,
  ITEM_DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(OBRIGACAO_PAGAR.DT_VENCIMENTO,10))

"""

# df_pagamentos = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_pagamentos = client.query(query).to_dataframe()

# COMMAND ----------

# for coluna in ['VL_UNITARIO_FISCAL', 'QT_ITEM_FISCAL', 'VL_CALCULO_INV', 'VL_CALCULO_OFO','VL_PARCELA']:

#   df_pagamentos = df_pagamentos.withColumn(coluna , col(coluna).cast('double'))

for coluna in [
  'VL_DOCUMENTO',
  'VL_SALDO',
  'VL_BRUTO',	
  'VL_PAGAMENTO',
  'VL_PAGAMENTO_AJUSTADO'
]:

  df_pagamentos[coluna] = df_pagamentos[coluna].astype(float)

# COMMAND ----------

# df_pagamentos.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'seara-auditoria-cfo-nonprod.capex_cashflow.capex_pagamentos').save()

# COMMAND ----------

df_pagamentos['MES_PREVISAO_PAGAMENTO'] = df_pagamentos['DT_PREVISAO_PAGAMENTO'].apply(lambda x: x.month if not pd.isnull(x) else x)

df_pagamentos['ANO_PREVISAO_PAGAMENTO'] = df_pagamentos['DT_PREVISAO_PAGAMENTO'].apply(lambda x: x.year if not pd.isnull(x) else x)

df_pagamentos['MES_DT_INTEGRACAO'] = df_pagamentos['DT_INTEGRACAO'].apply(lambda x: x.month if not pd.isnull(x) else x)

df_pagamentos['ANO_DT_INTEGRACAO'] = df_pagamentos['DT_INTEGRACAO'].apply(lambda x: x.year if not pd.isnull(x) else x)

de_para_nome_mes = {
  1: 'janeiro',
  2: 'fevereiro',
  3: 'março',
  4: 'abril',
  5: 'maio',
  6: 'junho',
  7: 'julho',
  8: 'agosto',
  9: 'setembro',
  10: 'outubro',
  11: 'novembro',
  12: 'dezembro'
}

df_pagamentos['NM_MES_DT_INTEGRACAO'] = df_pagamentos['MES_DT_INTEGRACAO'].apply(lambda x: de_para_nome_mes.get(x))

# COMMAND ----------

table_id = "capex_pagamentos"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_pagamentos, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# df_pagamentos_pandas = df_pagamentos.toPandas()

# COMMAND ----------

# df_pagamentos.to_excel('gs://capex_cashflow/pagamentos_investimentos.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Query informações dos investimentos

# COMMAND ----------

query = f"""
SELECT
  CD_EMPRESA,
  CD_FILIAL,
  CD_INVESTIMENTO,
  ID_SITUACAO,
  CD_CENTRO_CUSTO,
  DS_INVESTIMENTO,
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_PREVISAO_INICIO,10)) AS DT_PREVISAO_INICIO,
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_PREVISAO_FIM,10)) AS DT_PREVISAO_FIM,
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_ANO_PLANO,10)) AS DT_ANO_PLANO,
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_APROVACAO,10)) AS DT_APROVACAO,
  INVESTIMENTO.DS_JUSTIFICATIVA,
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_FECHAMENTO,10)) AS DT_FECHAMENTO,
  DS_JUSTIFICATIVA_FECHAMENTO,
  INVESTIMENTO.VL_COMPROMETIDO,
  INVESTIMENTO.VL_REALIZADO,
FROM
  `seara-analytics-prod.stg_erp_seara.investimento` INVESTIMENTO
WHERE
  PARSE_DATE('%d/%m/%Y', LEFT(INVESTIMENTO.DT_PREVISAO_INICIO,10)) >= '2019-01-01'
  -- CD_INVESTIMENTO = 6169
  -- AND CD_EMPRESA = 30
  -- AND CD_FILIAL = 228
"""

# df_investimentos = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_investimentos = client.query(query).to_dataframe()

# COMMAND ----------

# for coluna in ['VL_COMPROMETIDO', 'VL_REALIZADO']:

#   df_investimentos = df_investimentos.withColumn(coluna , col(coluna).cast('double'))

for coluna in ['VL_COMPROMETIDO', 'VL_REALIZADO']:

  df_investimentos[coluna] = df_investimentos[coluna].astype(float)

# COMMAND ----------

# df_investimentos.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'seara-auditoria-cfo-nonprod.capex_cashflow.capex_info_investimentos').save()

# COMMAND ----------

table_id = "capex_info_investimentos"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_investimentos, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# df_investimentos_pandas = df_investimentos.toPandas()

# COMMAND ----------

# df_investimentos.to_excel('gs://capex_cashflow/informacoes_investimentos.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Query OFs em aberto

# COMMAND ----------

query = """
SELECT
  *
FROM
  `seara-analytics-prod.stg_erp_seara.ordem_fornecimento_aberto`
"""

# df_ofs_aberto = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

df_ofs_aberto = client.query(query).to_dataframe()

df_ofs_aberto['DT_ORDEM_FORNECIMENTO'] = pd.to_datetime(df_ofs_aberto['DT_ORDEM_FORNECIMENTO'], format = '%d/%m/%Y %H:%M:%S').dt.date

# COMMAND ----------

#df_ofs_aberto.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'seara-auditoria-cfo-nonprod.capex_cashflow.capex_ofs_aberto').save()

# COMMAND ----------

table_id = "capex_ofs_aberto"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_ofs_aberto, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

df_data_execucao = pd.DataFrame(columns=['DT_EXECUCAO'])

df_data_execucao.loc[0] = hoje

table_id = "data_execucao_capex_cashflow"

table_ref = dataset.table(table_id)

job_config = bq.LoadJobConfig(
    write_disposition="WRITE_TRUNCATE",
)

job = client.load_table_from_dataframe(df_data_execucao, table_ref, job_config=job_config)

job.result()  # Waits for table load to complete.
print("Loaded dataframe to {}".format(table_ref.path))

# COMMAND ----------

# df_ofs_aberto_pandas = df_ofs_aberto.toPandas()

# COMMAND ----------

# df_ofs_aberto.to_excel('gs://capex_cashflow/ofs_aberto.xlsx', index = False)

# COMMAND ----------

# storage_client = storage.Client()
# bucket = storage_client.bucket('capex_cashflow')
# blob = bucket.blob('capex_cashflow.xlsx')
# with io.BytesIO() as output:
#     writer = pd.ExcelWriter(output, engine='xlsxwriter')
#     df_investimentos.to_excel(writer, sheet_name='infos_investimentos', index = False)
#     df_documentos.to_excel(writer, sheet_name='documentos', index = False)
#     df_pagamentos.to_excel(writer, sheet_name='pagamentos', index = False)
#     df_ofs_aberto.to_excel(writer, sheet_name='ofs_aberto', index = False)
#     writer.save()
#     output.seek(0)
#     blob.upload_from_file(output, content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
