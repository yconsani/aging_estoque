# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ### Instalando bibliotecas e definindo funções

# COMMAND ----------

# ! pip install gcsfs
# ! pip install google-cloud-bigquery
# ! pip install pandas_gbq
# ! pip install --upgrade google-cloud-storage
# ! pip install openpyxl
# ! pip install xlrd
# ! pip install pulp
# ! pip install googlemaps

# COMMAND ----------

import pyspark.sql.functions as F
from pyspark.sql.functions import col, to_timestamp, when, lit
from pyspark.sql.types import DoubleType, IntegerType, StringType, DateType, StructType, StructField
from pyspark.sql import Window
import datetime
from datetime import timedelta
import unicodedata
import numpy as np
import pandas as pd
import scipy.stats as stats
import pytz

from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

spark.conf.set("viewsEnabled","true")

from google.cloud import bigquery as bq
client = bq.Client()

tz = pytz.timezone('Brazil/East')

# COMMAND ----------

# função que para facilitar o carregamento de tabelas no bigquery

def carrega_tabela_bq(id_projeto, nome_dataset, nome_tabela, df, modo = 'WRITE_TRUNCATE'):
  
  table_id = nome_tabela
  
  dataset = bq.dataset.Dataset(f"{id_projeto}.{nome_dataset}")

  table_ref = dataset.table(table_id)

  job_config = bq.LoadJobConfig(
      write_disposition= modo,
  )

  job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)

  job.result()  # Waits for table load to complete.
  
#   df.to_gbq(
#     f'{nome_dataset}.{nome_tabela}',
#     project_id=id_projeto,
#     chunksize=None,
#     if_exists=modo
#   )
  
  return print(f'tabela {nome_tabela} carregada no local {id_projeto}.{nome_dataset}.{nome_tabela}')  #print(f"Loaded dataframe to {table_ref.path}")

# COMMAND ----------

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

print(f'hoje: {hoje}')

def encontra_mes_ref(data):
  
  mes_ref_atual = data.replace(day=1)
  
  mes_ref_anterior = (mes_ref_atual - datetime.timedelta(days = 1)).replace(day=1)
  
  if data.day == 1:
    
    return mes_ref_anterior
  
  else:
    
    return mes_ref_atual                                                            

# COMMAND ----------

final_do_mes_anterior = hoje.replace(day=1) - timedelta(days=1)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Tabela Itens

# COMMAND ----------

query = """
   SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ORIGEM.ID_ORIGEM,
    ORIGEM.DS_ORIGEM
    
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
LEFT JOIN
(
SELECT
  RV_LOW_VALUE AS ID_ORIGEM,
  -- RV_DOMAIN,
  RV_MEANING AS DS_ORIGEM
FROM
  `seara-analytics-prod.erp_seara.bcmg_ref_codes`
WHERE
  RV_DOMAIN = 'ITEM.ID_ORIGEM'
) AS ORIGEM
ON
  CAST(ORIGEM.ID_ORIGEM AS INT) = ITEM.ID_ORIGEM

"""

ITEM = client.query(query).to_dataframe()

# COMMAND ----------

df_gerencia_itens = pd.read_excel('gs://free_cash_flow/Contas a pagar/cadastro_gerencia_itens/CADASTRO_GERENCIA_ITENS.xlsx')

df_gerencia_itens = df_gerencia_itens.sort_values(by=['CD_ITEM', 'GERENCIA'], ascending = [False, False]).groupby(['CD_ITEM','GERENCIA']).first().reset_index()[['CD_ITEM', 'GERENCIA']]

# COMMAND ----------

ITEM = ITEM.merge(
  df_gerencia_itens,
  how = 'left',
  on = ['CD_ITEM']
)

ITEM['FILTRO_ITEM'] = ITEM['CD_ITEM'].astype(str) + ' - ' + ITEM['NM_ITEM']

# COMMAND ----------

ITEM['GERENCIA'] = ITEM['GERENCIA'].fillna('N/A')

# COMMAND ----------

carrega_tabela_bq("seara-financeiro-nonprod", "freecashflow", "dim_item", ITEM, 'WRITE_TRUNCATE')

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Conta 2010_2051

# COMMAND ----------

def consulta_1_conta_2010_2051(data_ref):

  query = f"""

  WITH BASE AS (

  SELECT
     OBRIGACAO_PAGAR.CD_DOCUMENTO_ORIGEM
   , OBRIGACAO_PAGAR.CD_EMPRESA_PAGAMENTO
   , OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO
   , SITUACAO_OBRIGACAO.DS_SITUACAO_OBRIGACAO
   , OBRIGACAO_PAGAR.VL_BRUTO
   , OBRIGACAO_PAGAR.NR_DOCUMENTO
   , OBRIGACAO_PAGAR.NR_SEQUENCIA
   , OBRIGACAO_PAGAR.CD_BASE_CREDOR
   , OBRIGACAO_PAGAR.DT_INCLUSAO
   , OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA
   , OBRIGACAO_PAGAR.CD_CENTRO_CUSTO
   , OBRIGACAO_PAGAR.DS_OBSERVACAO
   , OBRIGACAO_PAGAR.DT_PREVISAO_PAGAMENTO
   , OBRIGACAO_PAGAR.DT_PAGAMENTO
   , SUBSTR(CREDOR_OBRIGACAO.NM_CREDOR, 1, 40) as NM_CREDOR
   , OBRIGACAO_PAGAR.DT_EMISSAO
   , OBRIGACAO_PAGAR.DT_CONTABILIZACAO
   , OBRIGACAO_PAGAR.DT_ATUALIZACAO
   , ITEM_DOCUMENTO_RECEBIMENTO2.CD_ITEM
   , ITEM_DOCUMENTO_RECEBIMENTO2.VL_CALCULO_CUSTO
  FROM
    `seara-analytics-prod.stg_erp_seara.obrigacao_pagar` OBRIGACAO_PAGAR
    --ITEM_DOCUMENTO_RECEBIMENTO  ITEM_DOCUMENTO_RECEBIMENTO2,
    --DOCUMENTO_RECEBIMENTO,
    --OBRIGACAO_PAGAR_DOC_RCB
  right join `seara-analytics-prod.stg_erp_seara.obrigacao_pagar_doc_rcb`  OBRIGACAO_PAGAR_DOC_RCB
          on OBRIGACAO_PAGAR.NR_DOCUMENTO                     = OBRIGACAO_PAGAR_DOC_RCB.NR_DOCUMENTO_OBRIGACAO
         and OBRIGACAO_PAGAR.NR_SEQUENCIA                     = OBRIGACAO_PAGAR_DOC_RCB.NR_SEQUENCIA_OBRIGACAO
  right join `seara-analytics-prod.stg_erp_seara.documento_recebimento` DOCUMENTO_RECEBIMENTO
          on OBRIGACAO_PAGAR_DOC_RCB.CD_DOCUMENTO_RECEBIMENTO = DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO
         and OBRIGACAO_PAGAR_DOC_RCB.CD_BASE_FORNECEDOR       = DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR
         and OBRIGACAO_PAGAR_DOC_RCB.CD_ESTAB_FORNECEDOR      = DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR
         and OBRIGACAO_PAGAR_DOC_RCB.CD_SERIE_DOCUMENTO       = DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  right join `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` ITEM_DOCUMENTO_RECEBIMENTO2
          on DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR         = ITEM_DOCUMENTO_RECEBIMENTO2.CD_BASE_FORNECEDOR
         and DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO               = ITEM_DOCUMENTO_RECEBIMENTO2.CD_DOCUMENTO
         and DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR        = ITEM_DOCUMENTO_RECEBIMENTO2.CD_ESTAB_FORNECEDOR
         and DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO         = ITEM_DOCUMENTO_RECEBIMENTO2.CD_SERIE_DOCUMENTO
  right join `seara-analytics-prod.stg_erp_seara.credor_obrigacao` CREDOR_OBRIGACAO
          on CREDOR_OBRIGACAO.CD_BASE_CREDOR                  = OBRIGACAO_PAGAR.CD_BASE_CREDOR
         and CREDOR_OBRIGACAO.CD_ESTAB_CREDOR                 = OBRIGACAO_PAGAR.CD_ESTAB_CREDOR
  left join `seara-analytics-prod.stg_erp_seara.situacao_obrigacao_pagar` SITUACAO_OBRIGACAO
          on OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO = SITUACAO_OBRIGACAO.ID_SITUACAO_OBRIGACAO
  where
         OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO  IN  (5, 32, 45, 41, 54, 4, 18, 55)
         AND  PARSE_DATETIME('%d/%m/%Y %H:%M:%S', OBRIGACAO_PAGAR.DT_INCLUSAO)  <  '{data_ref}' -- 'Fornecedores em aberto até'
         AND (
               PARSE_DATETIME('%d/%m/%Y %H:%M:%S', OBRIGACAO_PAGAR.DT_PAGAMENTO)  >=  '{data_ref}' 
               OR OBRIGACAO_PAGAR.DT_PAGAMENTO  IS NULL
             )  -- 'Dt pagamento maior que:'
         AND OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA  IS NOT NULL  
         AND (
              (OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO  NOT IN  (4, 15))
              OR (
                  PARSE_DATETIME('%d/%m/%Y %H:%M:%S', OBRIGACAO_PAGAR.DT_CONTABILIZACAO) >  '{data_ref}' -- 'Fornecedores em aberto até'
                  AND OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO  IN  (4, 15)
                 )
             )

  UNION all (
  SELECT
             HIS_OBRIGACAO_PAGAR.CD_DOCUMENTO_ORIGEM
           , HIS_OBRIGACAO_PAGAR.CD_EMPRESA_PAGAMENTO
           , HIS_OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO
           , SITUACAO_OBRIGACAO.DS_SITUACAO_OBRIGACAO
           , HIS_OBRIGACAO_PAGAR.VL_BRUTO
           , HIS_OBRIGACAO_PAGAR.NR_DOCUMENTO
           , HIS_OBRIGACAO_PAGAR.NR_SEQUENCIA
           , HIS_OBRIGACAO_PAGAR.CD_BASE_CREDOR
           , HIS_OBRIGACAO_PAGAR.DT_INCLUSAO
           , HIS_OBRIGACAO_PAGAR.CD_FILIAL_RECEPTORA
           , HIS_OBRIGACAO_PAGAR.CD_CENTRO_CUSTO
           , HIS_OBRIGACAO_PAGAR.DS_OBSERVACAO
           , HIS_OBRIGACAO_PAGAR.DT_PREVISAO_PAGAMENTO
           , HIS_OBRIGACAO_PAGAR.DT_PAGAMENTO
           , SUBSTR(CREDOR_OBRIGACAO.NM_CREDOR, 1, 40) as NM_CREDOR
           , OBRIGACAO_PAGAR.DT_EMISSAO
           , OBRIGACAO_PAGAR.DT_CONTABILIZACAO
           , OBRIGACAO_PAGAR.DT_ATUALIZACAO
           , ITEM_DOCUMENTO_RECEBIMENTO2.CD_ITEM
           , ITEM_DOCUMENTO_RECEBIMENTO2.VL_CALCULO_CUSTO
  FROM
    --HIS_OBRIGACAO_PAGAR,
      `seara-analytics-prod.stg_erp_seara.obrigacao_pagar` OBRIGACAO_PAGAR
    --ITEM_DOCUMENTO_RECEBIMENTO  ITEM_DOCUMENTO_RECEBIMENTO2,
    --CREDOR_OBRIGACAO,
    --DOCUMENTO_RECEBIMENTO,
    --OBRIGACAO_PAGAR_DOC_RCB
  right join `seara-analytics-prod.stg_erp_seara.obrigacao_pagar_doc_rcb` OBRIGACAO_PAGAR_DOC_RCB
          on OBRIGACAO_PAGAR.NR_DOCUMENTO = OBRIGACAO_PAGAR_DOC_RCB.NR_DOCUMENTO_OBRIGACAO
         and OBRIGACAO_PAGAR.NR_SEQUENCIA = OBRIGACAO_PAGAR_DOC_RCB.NR_SEQUENCIA_OBRIGACAO
  right join `seara-analytics-prod.stg_erp_seara.documento_recebimento`DOCUMENTO_RECEBIMENTO
          on OBRIGACAO_PAGAR_DOC_RCB.CD_DOCUMENTO_RECEBIMENTO = DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO
         and OBRIGACAO_PAGAR_DOC_RCB.CD_BASE_FORNECEDOR       = DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR
         and OBRIGACAO_PAGAR_DOC_RCB.CD_ESTAB_FORNECEDOR      = DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR
         and OBRIGACAO_PAGAR_DOC_RCB.CD_SERIE_DOCUMENTO       = DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO
  inner join  `seara-analytics-prod.stg_erp_seara.credor_obrigacao` CREDOR_OBRIGACAO
          on OBRIGACAO_PAGAR.CD_BASE_CREDOR  = CREDOR_OBRIGACAO.CD_BASE_CREDOR
         and OBRIGACAO_PAGAR.CD_ESTAB_CREDOR = CREDOR_OBRIGACAO.CD_ESTAB_CREDOR
  inner join `seara-analytics-prod.stg_erp_seara.his_obrigacao_pagar` HIS_OBRIGACAO_PAGAR
          on HIS_OBRIGACAO_PAGAR.CD_BASE_CREDOR  = CREDOR_OBRIGACAO.CD_BASE_CREDOR 
         and HIS_OBRIGACAO_PAGAR.CD_ESTAB_CREDOR = CREDOR_OBRIGACAO.CD_ESTAB_CREDOR
  right join `seara-analytics-prod.stg_erp_seara.item_documento_recebimento` ITEM_DOCUMENTO_RECEBIMENTO2
          on DOCUMENTO_RECEBIMENTO.CD_BASE_FORNECEDOR  = ITEM_DOCUMENTO_RECEBIMENTO2.CD_BASE_FORNECEDOR
         and DOCUMENTO_RECEBIMENTO.CD_DOCUMENTO        = ITEM_DOCUMENTO_RECEBIMENTO2.CD_DOCUMENTO
         and DOCUMENTO_RECEBIMENTO.CD_ESTAB_FORNECEDOR = ITEM_DOCUMENTO_RECEBIMENTO2.CD_ESTAB_FORNECEDOR
         and DOCUMENTO_RECEBIMENTO.CD_SERIE_DOCUMENTO  = ITEM_DOCUMENTO_RECEBIMENTO2.CD_SERIE_DOCUMENTO
  left join `seara-analytics-prod.stg_erp_seara.situacao_obrigacao_pagar` SITUACAO_OBRIGACAO
          on OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO = SITUACAO_OBRIGACAO.ID_SITUACAO_OBRIGACAO
  where      HIS_OBRIGACAO_PAGAR.CD_TIPO_OBRIGACAO      =  5
         and HIS_OBRIGACAO_PAGAR.ID_SITUACAO_OBRIGACAO  =  11
         and PARSE_DATE('%d/%m/%Y', HIS_OBRIGACAO_PAGAR.DT_ATUALIZACAO) >= '{data_ref}' -- 'Dt pagamento maior que:'
         and PARSE_DATE('%d/%m/%Y', HIS_OBRIGACAO_PAGAR.DT_INCLUSAO) < '{data_ref}' -- 'Fornecedores em aberto até'
  )
  --  AND  HIS_OBRIGACAO_PAGAR.DT_ATUALIZACAO  >=  @variable('Dt pagamento maior que:')
  --  AND  HIS_OBRIGACAO_PAGAR.DT_INCLUSAO  <  @variable('Fornecedores em aberto até')

  ),

  BASE_RATEADA AS (
  SELECT
    *,
    SUM(VL_CALCULO_CUSTO) OVER(
        PARTITION BY
          CD_EMPRESA_PAGAMENTO,
          CD_EMPRESA_PAGAMENTO,
          CD_TIPO_OBRIGACAO,
          NR_DOCUMENTO,
          NR_SEQUENCIA,
          CD_BASE_CREDOR,
          DT_EMISSAO
    ) AS VL_CALCULO_CUSTO_TOTAL,
    SAFE_DIVIDE(VL_CALCULO_CUSTO , (SUM(VL_CALCULO_CUSTO) OVER(
        PARTITION BY
          CD_EMPRESA_PAGAMENTO,
          CD_EMPRESA_PAGAMENTO,
          CD_TIPO_OBRIGACAO,
          NR_DOCUMENTO,
          NR_SEQUENCIA,
          CD_BASE_CREDOR,
          DT_EMISSAO
    )) ) AS PR_CUSTO,
    ROW_NUMBER() OVER(
        PARTITION BY
          CD_EMPRESA_PAGAMENTO,
          CD_EMPRESA_PAGAMENTO,
          CD_TIPO_OBRIGACAO,
          NR_DOCUMENTO,
          NR_SEQUENCIA,
          CD_BASE_CREDOR,
          DT_EMISSAO
        ORDER BY
          CD_ITEM
    ) AS rn,
    COUNT(CD_ITEM) OVER(
        PARTITION BY
          CD_EMPRESA_PAGAMENTO,
          CD_EMPRESA_PAGAMENTO,
          CD_TIPO_OBRIGACAO,
          NR_DOCUMENTO,
          NR_SEQUENCIA,
          CD_BASE_CREDOR,
          DT_EMISSAO
    ) AS contagem_item
  FROM
    BASE
  )

  SELECT
      CD_EMPRESA_PAGAMENTO
      , CD_FILIAL_RECEPTORA	
      , CD_TIPO_OBRIGACAO
      , DS_SITUACAO_OBRIGACAO
      , CD_BASE_CREDOR
      , NM_CREDOR
      , CD_DOCUMENTO_ORIGEM
      , NR_DOCUMENTO	
      , NR_SEQUENCIA
      , CD_ITEM
      , CD_CENTRO_CUSTO
      , DS_OBSERVACAO
      -- , VL_BRUTO
      , CASE
          WHEN rn = 1 THEN ROUND(VL_BRUTO,2) ELSE NULL 
        END AS VL_BRUTO_BO
      , CASE
          WHEN (VL_CALCULO_CUSTO_TOTAL IS NOT NULL AND contagem_item > 1)
            THEN ROUND(VL_BRUTO * PR_CUSTO,2)
          WHEN rn = 1
            THEN ROUND(VL_BRUTO,2)
          ELSE NULL
        END AS VL_BRUTO_RATEADO  
      , DT_ATUALIZACAO
      , DT_INCLUSAO
      , DT_EMISSAO
      , DT_CONTABILIZACAO
      , DT_PREVISAO_PAGAMENTO
      , DT_PAGAMENTO
      , EXTRACT(MONTH FROM PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_PREVISAO_PAGAMENTO)) AS MES_DT_PREVISAO_PAGAMENTO
      , EXTRACT(YEAR FROM PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_PREVISAO_PAGAMENTO)) AS ANO_DT_PREVISAO_PAGAMENTO
      , VL_CALCULO_CUSTO	
      , VL_CALCULO_CUSTO_TOTAL	
      , PR_CUSTO
      , rn
      , contagem_item
  FROM
    BASE_RATEADA
  ORDER BY
    CD_EMPRESA_PAGAMENTO
    , CD_FILIAL_RECEPTORA	
    , CD_TIPO_OBRIGACAO
    , DS_SITUACAO_OBRIGACAO
    , CD_BASE_CREDOR
    , NM_CREDOR
    , CD_DOCUMENTO_ORIGEM
    , NR_DOCUMENTO	
    , NR_SEQUENCIA
    , CD_ITEM
    , VL_BRUTO_BO
  """

  # df_consulta_1 = client.query(query).to_dataframe()

  df_consulta_1 = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

  for coluna in ['DT_EMISSAO','DT_PREVISAO_PAGAMENTO', 'DT_PAGAMENTO', 'DT_ATUALIZACAO', 'DT_INCLUSAO', 'DT_CONTABILIZACAO']:

    df_consulta_1 = df_consulta_1.withColumn(coluna, F.to_date(col(coluna),"dd/MM/yyyy HH:mm:ss"))

  for coluna in ['VL_BRUTO_BO', 'VL_BRUTO_RATEADO']:

    df_consulta_1 = df_consulta_1.withColumn(coluna, F.round(col(coluna).cast(DoubleType()), 2))


  df_consulta_1 = df_consulta_1.withColumn(
    'DS_MES_DT_PREVISAO_PAGAMENTO',
    when(col('MES_DT_PREVISAO_PAGAMENTO') == 1, 'janeiro') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 2, 'fevereiro') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 3, 'março') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 4, 'abril') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 5, 'maio') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 6, 'junho') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 7, 'julho') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 8, 'agosto') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 9, 'setembro') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 10, 'outubro') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 11, 'novembro') \
    .when(col('MES_DT_PREVISAO_PAGAMENTO') == 12, 'dezembro') 
  )

  df_consulta_1 = df_consulta_1.withColumn('DT_REFERENCIA', lit(data_ref))
  
  mes_ref = encontra_mes_ref(data_ref)
  
  df_consulta_1 = df_consulta_1.withColumn('MES_REFERENCIA', lit(mes_ref))

  print('Atualizando a tabela no bigquery.')

  delete_statement = f"""

  DELETE seara-financeiro-nonprod.freecashflow.conta_2010_2051_consulta_1
  WHERE
    MES_REFERENCIA = '{mes_ref}'
    
  """

  query_job = client.query(delete_statement)

  query_job.result()

  df_consulta_1.write.format("bigquery").mode('append').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'seara-financeiro-nonprod.freecashflow.conta_2010_2051_consulta_1').save()
  
#   return df_consulta_1

# COMMAND ----------

# for data in [
#   datetime.date(2022,2,1),
#   datetime.date(2022,3,1),
#   datetime.date(2022,4,1)
# ]:

#   consulta_1_conta_2010_2051(data)

# COMMAND ----------

if 1 < hoje.day <= 7:
  
  consulta_1_conta_2010_2051(datetime.date(hoje.year, hoje.month, 1))

consulta_1_conta_2010_2051(hoje)

# COMMAND ----------

# Conciliação

# df = consulta_1_conta_2010_2051(datetime.date(2022,3,1))

# df = df.toPandas()

# df.to_excel('gs://free_cash_flow/Contas a pagar/base_fornec.materiais_gerais.xlsx', index = False)

# df_bo = pd.read_excel('gs://free_cash_flow/Contas a pagar/Fornec Mat Gerais _ Consolidado por Item V1.xlsx')

# df_bo = df_bo[[
#  'Emp.',
# #  'Unnamed: 1',
#  'Filial Rec',
#  'Tp Obrig.',
# #  'Unnamed: 4',
#  'CNPJ',
#  'Nm Credor',
#  'Obrigação',
#  'Doc Origem',
#  'Seq',
#  'Dt Inclusao',
# #  'Unnamed: 11',
#  'Dt Previsao Pag',
#  'Dt Emissao',
#  'Cd Item',
#  'Vl Bruto'
# ]]

# print('Diferença entre bigquery e qlik:', df['VL_BRUTO_RATEADO'].sum() - df_bo['Vl Bruto'].sum())

# df_agg = df.groupby([
#   'CD_EMPRESA_PAGAMENTO',	
#   'CD_FILIAL_RECEPTORA',
#   'CD_TIPO_OBRIGACAO',
#   'CD_BASE_CREDOR',
#   'CD_DOCUMENTO_ORIGEM',
#   'NR_DOCUMENTO',
# #   'NR_SEQUENCIA',
# #   'CD_ITEM'
# ]).agg({
#   'VL_BRUTO_BO': 'sum',
#   'VL_BRUTO_RATEADO': 'sum'
# }).reset_index()

# df_bo_agg = df_bo.groupby([
#   'Emp.',	
#   'Filial Rec',	
#   'Tp Obrig.',	
#   'CNPJ',	
#   'Obrigação',	
#   'Doc Origem',	
# #   'Seq',
# #   'Cd Item'
# ]).agg({
#   'Vl Bruto': 'sum'
# }).reset_index().rename(columns={
#   'Emp.': 'CD_EMPRESA_PAGAMENTO',	
#   'Filial Rec': 'CD_FILIAL_RECEPTORA',	
#   'Tp Obrig.': 'CD_TIPO_OBRIGACAO',	
#   'CNPJ': 'CD_BASE_CREDOR',	
#   'Obrigação': 'NR_DOCUMENTO',	
#   'Doc Origem': 'CD_DOCUMENTO_ORIGEM',	
# #   'Seq': 'NR_SEQUENCIA',
# #   'Cd Item': 'CD_ITEM',
#   'Vl Bruto': 'VL_BRUTO'
# })

# df_conciliacao = df_agg.merge(
#   df_bo_agg,
#   how = 'left',
#   on = ['CD_EMPRESA_PAGAMENTO','CD_FILIAL_RECEPTORA','CD_TIPO_OBRIGACAO','CD_BASE_CREDOR','CD_DOCUMENTO_ORIGEM','NR_DOCUMENTO']
# )

# df_conciliacao['VL_BRUTO_RATEADO'] = df_conciliacao['VL_BRUTO_RATEADO'].fillna(0)
# df_conciliacao['VL_BRUTO'] = df_conciliacao['VL_BRUTO'].fillna(0)

# df_conciliacao['DIFERENCA'] = abs(df_conciliacao['VL_BRUTO_BO'] - df_conciliacao['VL_BRUTO'])

# df_conciliacao.sort_values(by=['DIFERENCA'], ascending = False, inplace = True)

# df_conciliacao.rename(columns={'VL_BRUTO_BO': 'VL_BRUTO_PWBI', 'VL_BRUTO': 'VL_BRUTO_BO'}, inplace = True)

# df_conciliacao.head(100).to_excel('gs://free_cash_flow/Contas a pagar/conciliacao_forn_materiais_gerais.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Conta 2090_2599

# COMMAND ----------

def conta_2090_2599(data_ref):

  query = f"""

  SELECT DISTINCT
    CONCILIACAO_FRETE_PAGAR.CD_EMPRESA,
    CONCILIACAO_FRETE_PAGAR.CD_OPERACAO_TRANSPORTE,
    CONCILIACAO_FRETE_PAGAR.CD_FILIAL,
    CONCILIACAO_FRETE_PAGAR.CD_BASE_CONHECIMENTO,
    CONCILIACAO_FRETE_PAGAR.CD_ESTAB_CONHECIMENTO,
    -- FILIAL_CGC_VIAGEM_TRANSPORTE.NM_FILIAL,
    CONCILIACAO_FRETE_PAGAR.CD_CONHECIMENTO,
    CONCILIACAO_FRETE_PAGAR.CD_SERIE_CONHECIMENTO,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_ATUALIZACAO) DT_ATUALIZACAO,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_CONTABILIZACAO) DT_CONTABILIZACAO,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_EMISSAO) DT_EMISSAO,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_INCLUSAO) DT_INCLUSAO,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_INCLUSAO_OBR) DT_INCLUSAO_OBR,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_REFERENCIA) DT_REFERENCIA,
    CONCILIACAO_FRETE_PAGAR.DS_RECEBEDOR,
    CONCILIACAO_FRETE_PAGAR.NM_FILIAL,
    CONCILIACAO_FRETE_PAGAR.NM_EMPRESA,
    CONCILIACAO_FRETE_PAGAR.NM_TRANSPORTADOR,
    CONCILIACAO_FRETE_PAGAR.TP_FRETE,
    CONCILIACAO_FRETE_PAGAR.ID_SITUACAO,
    SITUACAO_OBRIGACAO.DS_SITUACAO_OBRIGACAO,
    CONCILIACAO_FRETE_PAGAR.ID_USUARIO,
    CONCILIACAO_FRETE_PAGAR.VL_LANCAMENTO,
    OPERACAO_TRANSPORTE3.NM_OPERACAO_TRANSPORTE,
    OPERACAO_TRANSPORTE.CD_MODALIDADE_TRANSPORTE,
    MODALIDADE_TRANSPORTE_LTCC.NM_MODALIDADE_TRANSPORTE,
    VIAGEM_TRANSPORTE.DT_FECHAMENTO_VIAGEM,
    -- VIAGEM_TRANSPORTE.CD_FILIAL,
    VIAGEM_TRANSPORTE.CD_VIAGEM_TRANSPORTE,
    CONHECIMENTO_FRETE.CD_CTRC_ORIGINAL,
    DATE(CONCILIACAO_FRETE_PAGAR.DT_VENCIMENTO_OBR_PGT) DT_VENCIMENTO_OBR_PGT,
    CONCILIACAO_FRETE_PAGAR.CD_CENTRO_CUSTO,
    CONCILIACAO_FRETE_PAGAR.CD_CONTA_CONTABIL,
    VIAGEM_TRANSPORTE.CD_BASE_TRANSPORTADOR
  FROM
    `seara-analytics-prod.stg_erp_seara.conciliacao_frete_pagar` CONCILIACAO_FRETE_PAGAR
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.conhecimento_frete` CONHECIMENTO_FRETE
  ON
    CONHECIMENTO_FRETE.CD_BASE_CONHECIMENTO=CONCILIACAO_FRETE_PAGAR.CD_BASE_CONHECIMENTO
    AND CONHECIMENTO_FRETE.CD_ESTAB_CONHECIMENTO=CONCILIACAO_FRETE_PAGAR.CD_ESTAB_CONHECIMENTO
    AND CONHECIMENTO_FRETE.CD_SERIE_CONHECIMENTO=CONCILIACAO_FRETE_PAGAR.CD_SERIE_CONHECIMENTO
    AND CONHECIMENTO_FRETE.CD_CONHECIMENTO=CONCILIACAO_FRETE_PAGAR.CD_CONHECIMENTO
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item_conhecimento_frete` ITEM_CONHECIMENTO_FRETE
  ON
    ITEM_CONHECIMENTO_FRETE.CD_BASE_CONHECIMENTO=CONHECIMENTO_FRETE.CD_BASE_CONHECIMENTO 
    AND ITEM_CONHECIMENTO_FRETE.CD_ESTAB_CONHECIMENTO=CONHECIMENTO_FRETE.CD_ESTAB_CONHECIMENTO
    AND ITEM_CONHECIMENTO_FRETE.CD_SERIE_CONHECIMENTO=CONHECIMENTO_FRETE.CD_SERIE_CONHECIMENTO
    AND ITEM_CONHECIMENTO_FRETE.CD_CONHECIMENTO=CONHECIMENTO_FRETE.CD_CONHECIMENTO
    AND ITEM_CONHECIMENTO_FRETE.NR_SEQUENCIA_HISTORICO=CONHECIMENTO_FRETE.NR_SEQUENCIA_HISTORICO
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.item_viagem_transporte` ITEM_VIAGEM_TRANSPORTE
  ON
    ITEM_VIAGEM_TRANSPORTE.CD_VIAGEM_TRANSPORTE=ITEM_CONHECIMENTO_FRETE.CD_VIAGEM_TRANSPORTE
    AND ITEM_VIAGEM_TRANSPORTE.CD_ITEM_VIAGEM_TRANSPORTE=ITEM_CONHECIMENTO_FRETE.CD_ITEM_VIAGEM_TRANSPORTE
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.viagem_transporte` VIAGEM_TRANSPORTE
  ON
    VIAGEM_TRANSPORTE.CD_VIAGEM_TRANSPORTE=ITEM_VIAGEM_TRANSPORTE.CD_VIAGEM_TRANSPORTE
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.operacao_transporte` OPERACAO_TRANSPORTE
  ON
    OPERACAO_TRANSPORTE.CD_OPERACAO_TRANSPORTE=VIAGEM_TRANSPORTE.CD_OPERACAO_TRANSPORTE
  INNER JOIN
    `seara-analytics-prod.stg_erp_seara.filial_cgc` FILIAL_CGC_VIAGEM_TRANSPORTE
  ON
    VIAGEM_TRANSPORTE.CD_FILIAL=FILIAL_CGC_VIAGEM_TRANSPORTE.CD_FILIAL
    AND VIAGEM_TRANSPORTE.CD_EMPRESA=FILIAL_CGC_VIAGEM_TRANSPORTE.CD_EMPRESA
  LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.operacao_transporte` OPERACAO_TRANSPORTE3
  ON
    CONCILIACAO_FRETE_PAGAR.CD_OPERACAO_TRANSPORTE=OPERACAO_TRANSPORTE3.CD_OPERACAO_TRANSPORTE
  LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.modalidade_transporte_ltcc` MODALIDADE_TRANSPORTE_LTCC
  ON
    OPERACAO_TRANSPORTE.CD_MODALIDADE_TRANSPORTE = MODALIDADE_TRANSPORTE_LTCC.CD_MODALIDADE_TRANSPORTE
  LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.situacao_obrigacao_pagar` SITUACAO_OBRIGACAO
  ON
    CAST(CONCILIACAO_FRETE_PAGAR.ID_SITUACAO AS INT64) = SITUACAO_OBRIGACAO.ID_SITUACAO_OBRIGACAO
  WHERE
    CONCILIACAO_FRETE_PAGAR.CD_EMPRESA  IN  (30, 36, 178) -- @variable('Empresa')
    AND  DATE(CONCILIACAO_FRETE_PAGAR.DT_INCLUSAO)  = '{data_ref}' -- @variable('1) Data Referência Relatório (dd/mm/aaaa)')
    AND  DATE(CONCILIACAO_FRETE_PAGAR.DT_EMISSAO)  <= '{data_ref}'  -- @variable('Data Emissão Menor ou igual a (dd/mm/aaaa)')
    AND  CONHECIMENTO_FRETE.DT_CANCELAMENTO_CONHECIMENTO  IS NULL  
    AND  CONCILIACAO_FRETE_PAGAR.TP_FRETE  =  'PG'
    AND  CONCILIACAO_FRETE_PAGAR.CD_CENTRO_CUSTO  =  2090
    AND  VIAGEM_TRANSPORTE.CD_BASE_TRANSPORTADOR  !=  2916265
  """
  
  df_conta_2090_2599 = client.query(query).to_dataframe()
  
  mes_ref = data_ref.replace(day = 1)
  
  df_conta_2090_2599['MES_REF'] = mes_ref
  
  print('Atualizando a tabela no bigquery.')

  delete_statement = f"""

  DELETE seara-financeiro-nonprod.freecashflow.conta_2090_2599_consulta_1
  WHERE
    MES_REF = '{mes_ref}'
    
  """

  query_job = client.query(delete_statement)

  query_job.result()
  
  carrega_tabela_bq("seara-financeiro-nonprod", "freecashflow", "conta_2090_2599_consulta_1", df_conta_2090_2599, 'WRITE_APPEND')

# COMMAND ----------

# datas = [
#   datetime.date(2022,2,28),
#   datetime.date(2022,3,31)
# ]

# for data in datas:
  
#   conta_2090_2599(data)

# COMMAND ----------

if hoje.day <= 7:
  
  conta_2090_2599(final_do_mes_anterior)
  
conta_2090_2599(hoje - timedelta(days = 1))
