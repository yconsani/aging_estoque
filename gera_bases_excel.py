# Databricks notebook source
# !pip install openpyxl
# !pip install fsspec
# !pip install gcsfs
# !pip install xlsxwriter
# !pip install bigquery

# COMMAND ----------

import pandas as pd
import numpy as np
import datetime
from datetime import timedelta
import numpy as np
import pytz

from google.cloud import bigquery as bq
from google.cloud import storage
client = bq.Client()

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

data_ref = hoje - timedelta(days = 1)

# COMMAND ----------

data_de_corte = hoje - timedelta(days = 60)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### OFs em aberto

# COMMAND ----------

query = """

SELECT * FROM `seara-auditoria-cfo-nonprod.capex_cashflow.capex_ofs_aberto`

"""

df = client.query(query).to_dataframe()

# COMMAND ----------

nome_arquivo = f'gs://capex_cashflow/arquivos_capex/ofs_em_aberto_{data_ref}.xlsx'

df.to_excel(nome_arquivo, index = False)

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Pagamentos Capex

# COMMAND ----------

query = """

SELECT * FROM `seara-auditoria-cfo-nonprod.capex_cashflow.capex_pagamentos`

"""

df = client.query(query).to_dataframe()

# COMMAND ----------

nome_arquivo = f'gs://capex_cashflow/arquivos_capex/capex_pagamentos_{data_ref}.xlsx'

df.to_excel(nome_arquivo, index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Executa limpeza de arquivos antigos

# COMMAND ----------

projeto = 'seara-financeiro-nonprod'

nome_bucket = 'capex_cashflow'

# COMMAND ----------

storage_client = storage.Client(project = projeto)

bucket = storage_client.bucket(nome_bucket)

blobs = storage_client.list_blobs(nome_bucket)

for blob in blobs:
  
  data_arquivo = blob.name.replace('arquivos_capex/','').replace('capex_pagamentos_','').replace('ofs_em_aberto_', '').replace('.xlsx', '')
  
  try:
  
    data_arquivo = datetime.datetime.strptime(data_arquivo, '%Y-%m-%d').date()
    
    if data_arquivo < data_de_corte:
      
      blob.delete()
    
  except:
    
    continue
