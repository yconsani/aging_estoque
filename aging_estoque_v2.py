# Databricks notebook source
# !pip install openpyxl
# !pip install fsspec
# !pip install gcsfs
# !pip install bigquery

# COMMAND ----------

from pyspark.sql.functions import *
from pyspark.sql.types import DoubleType, IntegerType, StringType, DateType

import datetime
import pytz
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate()
spark = SparkSession(sc)

spark.conf.set("viewsEnabled","true")

tz = pytz.timezone('Brazil/East')

hoje = datetime.datetime.today().replace(tzinfo=pytz.utc).astimezone(tz).date()

# COMMAND ----------

hoje = datetime.datetime.today().date()

data_ref = hoje - datetime.timedelta(days=2)

inicio_do_mes = hoje.replace(day = 1)

inicio_mes_anterior = (inicio_do_mes - datetime.timedelta(days =1)).replace(day=1)

inicio_mes_anterior_anterior = (inicio_mes_anterior - datetime.timedelta(days =1)).replace(day=1)

# COMMAND ----------

query = """
SELECT 
  MIN(PARSE_DATETIME('%d/%m/%Y', DT_FECHAMENTO_VALORIZACAO)) DT_FECHAMENTO_VALORIZACAO
FROM 
  `seara-analytics-prod.stg_erp_seara.rotina_estoque`
"""

df_data_custeio = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# data_custeio = pd.to_datetime(df_data_custeio['DT_FECHAMENTO_VALORIZACAO'].str.strip(), format = '%d/%m/%Y').min().date()

# COMMAND ----------

data_custeio = datetime.datetime.strptime((df_data_custeio.collect()[0]['DT_FECHAMENTO_VALORIZACAO'])[:10], '%Y-%m-%d').date()

# COMMAND ----------

# if hoje.day >= 10:
  
#   data_custeio = inicio_mes_anterior

# else:
  
#   data_custeio = inicio_mes_anterior_anterior

# COMMAND ----------

query = """
SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ITEM.CD_NEGOCIO_COMERCIAL,
    NEGOCIO_COMERCIAL.NM_NEGOCIO_COMERCIAL,
    tipo_aclimatacao_produto.NM_TIPO_ACLIMATACAO_PRO
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.negocio_comercial` NEGOCIO_COMERCIAL 
ON
  NEGOCIO_COMERCIAL.CD_NEGOCIO_COMERCIAl = ITEM.CD_NEGOCIO_COMERCIAL
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` tipo_aclimatacao_produto
ON
  tipo_aclimatacao_produto.CD_TIPO_ACLIMATACAO_PRO = ITEM.CD_TIPO_ACLIMATACAO
"""

ITEM = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

query = f"""
WITH FILIAIS AS (

    WITH A AS (
        SELECT
            CD_EMPRESA,
            CD_FILIAL,
            NM_FILIAL,
            DT_ATUALIZACAO,
            ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
        FROM
            `seara-analytics-prod.stg_erp_seara.filial_cgc`
    )

SELECT * FROM A WHERE A.rn = 1
)

SELECT
    CD_EMPRESA,
    CD_FILIAL, 
    NM_FILIAL 
FROM 
    FILIAIS
WHERE
    CD_FILIAL IN (
        SELECT
            DISTINCT CD_FILIAL
        FROM
        `seara-analytics-prod.stg_erp_seara.scf_producao_filial`
        WHERE
            ID_SITUACAO = 1
        )
ORDER BY 
    CD_FILIAL
"""

df_filiais = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

lista_filiais = [x['CD_FILIAL'] for x in df_filiais.select(['CD_FILIAL']).collect()]

lista_filiais = lista_filiais + [405, 130, 120]

filiais = str(lista_filiais).replace('[', '').replace(']', '')

print('filiais: ', filiais)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Diretorias Adjuntas

# COMMAND ----------

df_cad_funcionarios = spark.read.format('parquet').load('gs://seara-us-dev-absent-data/base_cad_funcionarios')

# COMMAND ----------

df_aves_pesadas = df_cad_funcionarios.filter(
  (col('DATA_CAD') == inicio_do_mes)
  &(col('DIRETORIA_EXECUTIVA') == 'NEG. AVES PESADAS')
  &(col('DIRETORIA_ADJUNTA').isin([
    'AVES PESADAS - C. OESTE, BA E MG',
    'AVES PESADAS - PR',
    'AVES PESADAS - RS E SC',
    'AVES PESADAS - SP'
  ]))
  &(col('CD_EMPRESA_HIERARQUIA').isin([30,36]))
  &(~(col('CD_FILIAL_HIERARQUIA').isin([555,90])))
).select(
  'CD_EMPRESA_HIERARQUIA', 'CD_FILIAL_HIERARQUIA', 'DIRETORIA_ADJUNTA'
).distinct().orderBy('CD_EMPRESA_HIERARQUIA','CD_FILIAL_HIERARQUIA')

df_aves_pesadas = df_aves_pesadas.withColumnRenamed('CD_EMPRESA_HIERARQUIA', 'CD_EMPRESA').withColumnRenamed('CD_FILIAL_HIERARQUIA', 'CD_FILIAL')

# COMMAND ----------

df_aves_pesadas = df_aves_pesadas.withColumn('CD_EMPRESA', col('CD_EMPRESA').cast('int'))

# COMMAND ----------

df_aves_pesadas = df_aves_pesadas.groupBy('CD_EMPRESA','CD_FILIAL').agg(first('DIRETORIA_ADJUNTA').alias('DIRETORIA_ADJUNTA'))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Estoque FIFO

# COMMAND ----------

query = f"""

WITH 

ITEM AS (
SELECT
    ITEM.CD_ITEM,
    ITEM.NM_ITEM,
    ITEM.DS_ITEM,
    ITEM.CD_CLASSE_ITEM,
    CLASSE_ITEM.NM_CLASSE,
    ITEM.CD_GRUPO_ITEM,
    GRUPO_ITEM.NM_GRUPO,
    ITEM.CD_SUBGRUPO_ITEM,
    SUBGRUPO_ITEM.NM_SUBGRUPO,
    --ITEM.CD_FAMILIA_ITEM,
    ITEM.CD_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.DS_FAMILIA_PRODUTO,
    FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO,
    FAMILIA_SINTETICA.DS_FAM_SINTETICA_PRO,
    FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO,
    FAMILIA_ANALITICA.DS_FAM_ANALITICA_PRO,
    CLASSIFICACAO_CARNE.DS_CLASSIFICACAO,
    ITEM.CD_DESTINO_FINAL,
    REF_CODES.DS_DESTINO_FINAL,
    MERCADO_EXPORTACAO.CD_MERCADO,
    MERCADO_EXPORTACAO.NM_MERCADO,
    ITEM.CD_NEGOCIO,
    NEGOCIO.NM_NEGOCIO,
    ITEM.CD_NEGOCIO_COMERCIAL,
    NEGOCIO_COMERCIAL.NM_NEGOCIO_COMERCIAL,
    tipo_aclimatacao_produto.NM_TIPO_ACLIMATACAO_PRO,
    ITEM.QT_DIAS_VALIDADE
FROM
    `seara-analytics-prod.stg_erp_seara.item` AS ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classe_item` AS CLASSE_ITEM
ON
    ITEM.CD_GRUPO_ITEM = CLASSE_ITEM.CD_GRUPO_ITEM
    AND ITEM.CD_SUBGRUPO_ITEM = CLASSE_ITEM.CD_SUBGRUPO_ITEM
    AND ITEM.CD_CLASSE_ITEM = CLASSE_ITEM.CD_CLASSE_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.grupo_item` AS GRUPO_ITEM
ON
    ITEM.CD_GRUPO_ITEM = GRUPO_ITEM.CD_GRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.subgrupo_item` AS SUBGRUPO_ITEM
ON
    SUBGRUPO_ITEM.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND SUBGRUPO_ITEM.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM

LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_produto` AS FAMILIA_PRODUTO  
ON
    FAMILIA_PRODUTO.CD_FAMILIA_PRODUTO = ITEM.CD_FAMILIA_PRODUTO
    --AND FAMILIA_PRODUTO.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    --AND FAMILIA_PRODUTO.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_sintetica_produto` AS FAMILIA_SINTETICA
ON
    FAMILIA_SINTETICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_SINTETICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_SINTETICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.familia_analitica_produto` AS FAMILIA_ANALITICA
ON
    FAMILIA_ANALITICA.CD_FAM_SINTETICA_PRO = FAMILIA_PRODUTO.CD_FAM_SINTETICA_PRO
    AND FAMILIA_ANALITICA.CD_FAM_ANALITICA_PRO = FAMILIA_PRODUTO.CD_FAM_ANALITICA_PRO
    AND FAMILIA_ANALITICA.CD_GRUPO_ITEM = ITEM.CD_GRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_SUBGRUPO_ITEM = ITEM.CD_SUBGRUPO_ITEM
    AND FAMILIA_ANALITICA.CD_NIVEL_IDZ_PRODUTO = FAMILIA_PRODUTO.CD_NIVEL_IDZ_PRODUTO
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_item_carne` AS CLASSIFICACAO_ITEM_CARNE
ON
    CLASSIFICACAO_ITEM_CARNE.CD_ITEM = ITEM.CD_ITEM
LEFT JOIN 
    `seara-analytics-prod.stg_erp_seara.classificacao_carne` AS CLASSIFICACAO_CARNE
ON
    CLASSIFICACAO_CARNE.CD_CLASSIFICACAO = CLASSIFICACAO_ITEM_CARNE.CD_CLASSIFICACAO
LEFT JOIN
    (
        SELECT
          CAST(RV_LOW_VALUE AS INT) AS CD_DESTINO_FINAL,
          RV_MEANING AS DS_DESTINO_FINAL
        FROM
          `seara-analytics-prod.stg_erp_seara.bcmg_ref_codes`
        WHERE
          RV_DOMAIN = 'ITEM.CD_DESTINO_FINAL'
    ) AS REF_CODES
ON
    ITEM.CD_DESTINO_FINAL = REF_CODES.CD_DESTINO_FINAL
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.mercado_exportacao` MERCADO_EXPORTACAO
ON
    ITEM.CD_MERCADO = MERCADO_EXPORTACAO.CD_MERCADO
LEFT JOIN
    `seara-analytics-prod.stg_erp_seara.negocio` NEGOCIO
ON
    ITEM.CD_NEGOCIO = NEGOCIO.CD_NEGOCIO
LEFT JOIN 
  `seara-analytics-prod.stg_erp_seara.negocio_comercial` NEGOCIO_COMERCIAL 
ON
  NEGOCIO_COMERCIAL.CD_NEGOCIO_COMERCIAl = ITEM.CD_NEGOCIO_COMERCIAL
LEFT JOIN
  `seara-analytics-prod.stg_erp_seara.tipo_aclimatacao_produto` tipo_aclimatacao_produto
ON
  tipo_aclimatacao_produto.CD_TIPO_ACLIMATACAO_PRO = ITEM.CD_TIPO_ACLIMATACAO
),

ESTOQUES AS (
  SELECT
      A.CD_EMPRESA, 
      A.CD_FILIAL,
      D.NM_FILIAL,
      CONCAT(A.CD_FILIAL, ' - ',D.NM_FILIAL) AS FILTRO_FILIAL,
      A.CD_LOCAL_ESTOQUE,
      C.DS_LOCAL_ESTOQUE,
      CONCAT(CAST(A.CD_LOCAL_ESTOQUE AS STRING),' - ',C.DS_LOCAL_ESTOQUE) AS FILTRO_LOCAL_ESTOQUE,
      -- A.CD_SUBLOCAL_ESTOQUE,
      --  A.CD_EMPRESA_ORIGEM,
      -- A.CD_FILIAL_ORIGEM,
      A.CD_ITEM,
      ITEM.NM_ITEM,
      CONCAT(CAST(A.CD_ITEM AS STRING),' - ', ITEM.NM_ITEM) AS FILTRO_ITEM,
      A.QT_PESO,
      A.DT_REFERENCIA,
      A.CD_PALETE,
      A.DS_END_PALLET,
      A.DT_ATUALIZACAO,
      A.DT_ENTRADA,
      A.DT_ENTRADA_LOCAL_ESTOQUE,
      A.DT_FABRICACAO,
      A.DT_VALIDADE,
      ITEM.QT_DIAS_VALIDADE,
      ITEM.NM_GRUPO,
      ITEM.NM_SUBGRUPO,
      ITEM.NM_CLASSE,
      ITEM.DS_DESTINO_FINAL,
      ITEM.NM_NEGOCIO,
      ITEM.NM_NEGOCIO_COMERCIAL,
      ITEM.NM_TIPO_ACLIMATACAO_PRO,
      ROUND(100*SAFE_DIVIDE(DATE_DIFF(PARSE_DATETIME('%d/%m/%Y %H:%M:%S',A.DT_REFERENCIA),PARSE_DATETIME('%d/%m/%Y %H:%M:%S', A.DT_FABRICACAO), DAY), ITEM.QT_DIAS_VALIDADE),2) AS SHELF,
      ROUND(DATE_DIFF(PARSE_DATETIME('%d/%m/%Y %H:%M:%S',A.DT_REFERENCIA),PARSE_DATETIME('%d/%m/%Y %H:%M:%S', A.DT_FABRICACAO), DAY),2) AS AGING_DIAS

  FROM 
      `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia` AS A
  LEFT JOIN
      ITEM
  ON
      A.CD_ITEM = ITEM.CD_ITEM
  LEFT JOIN
      `seara-analytics-prod.stg_erp_seara.local_estoque` AS C
  ON
      A.CD_LOCAL_ESTOQUE = C.CD_LOCAL_ESTOQUE

  LEFT JOIN
      (
          WITH FILIAIS AS (

              WITH A AS (
                  SELECT
                      CD_FILIAL,
                      NM_FILIAL,
                      DT_ATUALIZACAO,
                      ROW_NUMBER() OVER(PARTITION BY CD_FILIAL ORDER BY PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_ATUALIZACAO) DESC) AS rn
                  FROM
                      `seara-analytics-prod.stg_erp_seara.filial_cgc`
              )

          SELECT CD_FILIAL, NM_FILIAL FROM A WHERE A.rn = 1
          )

          SELECT 
              CD_FILIAL, 
              NM_FILIAL 
          FROM 
              FILIAIS
      ) AS D
  ON
      D.CD_FILIAL = A.CD_FILIAL
  WHERE 
      PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_REFERENCIA) = '{data_ref}'
      --AND CD_LOCAL_ESTOQUE IN (1,100)
)        

SELECT
    ESTOQUES.*,
    CASE
            WHEN SHELF <= 33 THEN 'VERDE'
            WHEN (SHELF > 33) AND (SHELF <= 60) THEN 'AMARELO'
            WHEN (SHELF > 60) AND (SHELF <= 80) THEN 'LARANJA'
            WHEN (SHELF > 80) THEN 'VERMELHO'
            END AS COR_FAIXA_PR_VALIDADE,
    CASE
            WHEN SHELF <= 23 THEN 'VERDE CLARO'
            WHEN (SHELF > 23) AND (SHELF <= 33) THEN 'VERDE ESCURO'
            WHEN (SHELF > 33) AND (SHELF <= 50) THEN 'AMARELO CLARO'
            WHEN (SHELF > 50) AND (SHELF <= 60) THEN 'AMARELO ESCURO'
            WHEN (SHELF > 60) AND (SHELF <= 80) THEN 'LARANJA'
            WHEN (SHELF > 80) THEN 'VERMELHO'
            END AS COR_FAIXA_PR_VALIDADE_SEGMENTADO
FROM 
    ESTOQUES
"""

df_estoque_fifo = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

for coluna in ['CD_EMPRESA','CD_FILIAL','CD_ITEM', 'CD_PALETE']:

  df_estoque_fifo = df_estoque_fifo.withColumn(coluna, col(coluna).cast('int'))

# COMMAND ----------

# df_estoque_fifo = df_estoque_fifo.filter(
#   col('CD_LOCAL_ESTOQUE').isin([1,1055])
# )

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.withColumn('DT_REFERENCIA', to_date(col('DT_REFERENCIA'), 'dd/MM/yyyy HH:mm:ss'))

# COMMAND ----------

def retorna_inicio_do_mes(data):
     
  return data.replace(day = 1)

retorna_inicio_do_mes_UDF = udf(lambda x: retorna_inicio_do_mes(x), DateType())

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.withColumn('AGING_DIAS', when(col('AGING_DIAS') < 0, 0).otherwise(col('AGING_DIAS')))

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.withColumn('SHELF_X_PESO', col('SHELF')*col('QT_PESO')).withColumn('AGING_DIAS_X_PESO', col('AGING_DIAS')*col('QT_PESO'))

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.filter(col('QT_PESO') != 0)

# COMMAND ----------

def atribui_bucket(valor, largura_bin, valor_maximo):
  
  if valor >= valor_maximo:
    
    return f'Acima de {valor_maximo}'
  
  else:
      
    for limite_bin in range(0,valor_maximo,largura_bin):
      
      if (valor >= limite_bin)&(valor < limite_bin + largura_bin):
        
        return f'{limite_bin} a {limite_bin + largura_bin}'
      

atribui_bucket_UDF = udf(lambda x,y,z: atribui_bucket(x,y,z), StringType())

# COMMAND ----------

largura_bin = 15
valor_maximo = 180

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.withColumn('BIN_AGING_DIAS', atribui_bucket_UDF(col('AGING_DIAS'),lit(largura_bin),lit(valor_maximo)))

# COMMAND ----------

def atribui_ordem_bucket(valor, largura_bin, valor_maximo):
  
  if valor >= valor_maximo:
    
    return valor_maximo+largura_bin
  
  else:
      
    for limite_bin in range(0,valor_maximo,largura_bin):
      
      if (valor >= limite_bin)&(valor < limite_bin + largura_bin):
        
        return limite_bin


atribui_ordem_bucket_UDF = udf(lambda x,y,z: atribui_ordem_bucket(x,y,z), IntegerType())

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.withColumn('ORDEM_BIN', atribui_ordem_bucket_UDF(col('AGING_DIAS'),lit(largura_bin),lit(valor_maximo)))

# COMMAND ----------

df_estoque_fifo = df_estoque_fifo.join(
  df_aves_pesadas,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL']
)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Valor unitário

# COMMAND ----------

query = f"""
SELECT
  CD_EMPRESA_ORIGEM AS CD_EMPRESA,
  CD_FILIAL_ORIGEM AS CD_FILIAL,
  CD_ITEM_ORIGEM AS CD_ITEM,
  -- ROUND(SUM(QT_ITEM),2) QT_ITEM,
  -- ROUND(SUM(VL_ITEM),2) VL_ITEM,
  ROUND(SAFE_DIVIDE(SUM(VL_ITEM),SUM(QT_ITEM)),2) VL_UNITARIO
FROM
  `seara-analytics-prod.stg_erp_seara.posicao_estoque_valorizado`
WHERE
  LEFT(CAST(DT_MESANO_REFERENCIA AS STRING),10) = '{str(data_custeio)}'
--   AND CD_EMPRESA_ORIGEM = 30
--   AND CD_FILIAL_ORIGEM = 892
--   AND CD_ITEM_ORIGEM = 9741
GROUP BY 
  CD_EMPRESA_ORIGEM,
  CD_FILIAL_ORIGEM,
  CD_ITEM_ORIGEM
ORDER BY 
  CD_EMPRESA_ORIGEM,
  CD_FILIAL_ORIGEM,
  CD_ITEM_ORIGEM
"""

df_valor = spark.read.format("bigquery").option("materializationDataset", "databricks_temp_dataset").option("query", query).load()

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Tabela consolidada

# COMMAND ----------

df_estoque_consolidado = df_estoque_fifo.alias('df_estoque_fifo')

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.join(
  df_valor,
  how = 'left',
  on = ['CD_EMPRESA','CD_FILIAL','CD_ITEM']
).orderBy('CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_LOCAL_ESTOQUE')

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('VL_ITEM', col('QT_PESO')*col('VL_UNITARIO'))

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('BIN_AGING_DIAS', when(col('BIN_AGING_DIAS').isNull(), 'N/A').otherwise(col('BIN_AGING_DIAS')))

df_estoque_consolidado = df_estoque_consolidado.withColumn('ORDEM_BIN', when(col('ORDEM_BIN').isNull(), valor_maximo+2*largura_bin).otherwise(col('ORDEM_BIN')))

# COMMAND ----------

df_estoque_consolidado.createOrReplaceTempView("df_consolidado")

df_estoque_consolidado = spark.sql(
    """
      SELECT 
        *, 
       ROUND(PERCENT_RANK() OVER( 
           PARTITION BY NM_NEGOCIO
           ORDER BY VL_UNITARIO
         ),2) AS PERCENTIL_VL_UNITARIO,
        ROUND(PERCENT_RANK() OVER( 
           PARTITION BY NM_NEGOCIO
           ORDER BY VL_ITEM
         ),2) AS PERCENTIL_VL_SALDO
      FROM 
       df_consolidado
     """
)

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('SCORE', when(col('AGING_DIAS') <= 180, col('AGING_DIAS') + 100*col('PERCENTIL_VL_UNITARIO') + 100*col('PERCENTIL_VL_SALDO')) \
                                                           .when(col('AGING_DIAS') > 180, lit(180) + 100*col('PERCENTIL_VL_UNITARIO') + 100*col('PERCENTIL_VL_SALDO')) \
                                                           .otherwise(0))

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('FLAG_ITEM_CRITICO', when(col('SCORE') >= 300, 'Score Elevado')\
                                                           .when((col('SCORE') >=250)&(col('SCORE') < 300), 'Score Moderado')
                                                           .otherwise('Não é critico'))

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('COR_SHELF', when(col('SHELF') <= 33, 'VERDE') \
                                                           .when((col('SHELF') > 33)&(col('SHELF') <= 60), 'AMARELO') \
                                                           .when((col('SHELF') > 60)&(col('SHELF') <= 80), 'LARANJA') \
                                                           .when((col('SHELF') > 80), 'VERMELHO')                                                          
)

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('DESTINO_FINAL_INDICADOR', when(col('DS_DESTINO_FINAL').isin([
  'Mercado Externo'
]), 'MERCADO EXTERNO').when(col('DS_DESTINO_FINAL') != 'Mercado Externo', 
  'MERCADO INTERNO')
)

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn('ACLIMATACAO_INDICADOR', when(col('NM_TIPO_ACLIMATACAO_PRO').isin(['CONGELADO', 'IQF']), 'CONGELADO/IQF') \
                                                          .when(col('NM_TIPO_ACLIMATACAO_PRO').isin(['AMBIENTE', 'RESFRIADO']), 'RESFRIADO/AMBIENTE') 
                                                          )

# COMMAND ----------

# df_estoque_consolidado.filter(
#  (~col('DESTINO_FINAL_INDICADOR').isNull())
#   &(~col('ACLIMATACAO_INDICADOR').isNull())
# ).groupby('DESTINO_FINAL_INDICADOR','ACLIMATACAO_INDICADOR').agg(
#   expr('percentile(AGING_DIAS, array(0.33))')[0].alias('%25'),
#   expr('percentile(AGING_DIAS, array(0.50))')[0].alias('%50'),
#   expr('percentile(AGING_DIAS, array(0.75))')[0].alias('%75'),
#   expr('percentile(AGING_DIAS, array(0.90))')[0].alias('%90'),
#   expr('percentile(AGING_DIAS, array(1))')[0].alias('%100')
# ).orderBy('DESTINO_FINAL_INDICADOR','ACLIMATACAO_INDICADOR').display()

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
  'COR_AGING',
  # MERCADO EXTERNO E CONGELADO/IQF
  when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 45)
    , 'VERDE'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 90)
    , 'AMARELO'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 120)
    , 'LARANJA'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') > 120)
    , 'VERMELHO'
  ) \
  # MERCADO EXTERNO E RESFRIADO/AMBIENTE
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 30)
    , 'VERDE'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 60)
    , 'AMARELO'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 90)
    , 'LARANJA'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO EXTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') > 90)
    , 'VERMELHO'
  ) \
  # MERCADO INTERNO E CONGELADO/IQF
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 30)
    , 'VERDE'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 60)
    , 'AMARELO'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') <= 90)
    , 'LARANJA'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'CONGELADO/IQF')
    &(col('AGING_DIAS') > 90)
    , 'VERMELHO'
  ) \
  # MERCADO INTERNO E RESFRIADO/AMBIENTE
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 21)
    , 'VERDE'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 45)
    , 'AMARELO'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') <= 70)
    , 'LARANJA'
  ) \
  .when(
    (col('DESTINO_FINAL_INDICADOR') == 'MERCADO INTERNO')
    &(col('ACLIMATACAO_INDICADOR') == 'RESFRIADO/AMBIENTE')
    &(col('AGING_DIAS') > 70)
    , 'VERMELHO'
  ) \
)

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
  'COR_INDICADOR_AS',
  # SHELF VERDE
  when(
    (col('COR_SHELF') == 'VERDE')
    &(col('COR_AGING') == 'VERDE')
    , 'VERDE'
  ) \
  .when(
    (col('COR_SHELF') == 'VERDE')
    &(col('COR_AGING') == 'AMARELO')
    , 'AMARELO'
  ) \
  .when(
    (col('COR_SHELF') == 'VERDE')
    &(col('COR_AGING') == 'LARANJA')
    , 'LARANJA'
  ) \
  .when(
    (col('COR_SHELF') == 'VERDE')
    &(col('COR_AGING') == 'VERMELHO')
    , 'VERMELHO'
  ) \
  # SHELF AMARELO
  .when(
    (col('COR_SHELF') == 'AMARELO')
    &(col('COR_AGING') == 'VERDE')
    , 'AMARELO'
  ) \
  .when(
    (col('COR_SHELF') == 'AMARELO')
    &(col('COR_AGING') == 'AMARELO')
    , 'AMARELO'
  ) \
  .when(
    (col('COR_SHELF') == 'AMARELO')
    &(col('COR_AGING') == 'LARANJA')
    , 'LARANJA'
  ) \
  .when(
    (col('COR_SHELF') == 'AMARELO')
    &(col('COR_AGING') == 'VERMELHO')
    , 'VERMELHO'
  ) \
  # SHELF LARANJA
  .when(
    (col('COR_SHELF') == 'LARANJA')
    &(col('COR_AGING') == 'VERDE')
    , 'LARANJA'
  ) \
  .when(
    (col('COR_SHELF') == 'LARANJA')
    &(col('COR_AGING') == 'AMARELO')
    , 'LARANJA'
  ) \
  .when(
    (col('COR_SHELF') == 'LARANJA')
    &(col('COR_AGING') == 'LARANJA')
    , 'LARANJA'
  ) \
  .when(
    (col('COR_SHELF') == 'LARANJA')
    &(col('COR_AGING') == 'VERMELHO')
    , 'VERMELHO'
  ) \
  # SHELF VERMELHO
  .when(
    (col('COR_SHELF') == 'VERMELHO')
    &(col('COR_AGING') == 'VERDE')
    , 'VERMELHO'
  ) \
  .when(
    (col('COR_SHELF') == 'VERMELHO')
    &(col('COR_AGING') == 'AMARELO')
    , 'VERMELHO'
  ) \
  .when(
    (col('COR_SHELF') == 'VERMELHO')
    &(col('COR_AGING') == 'LARANJA')
    , 'VERMELHO'
  ) \
  .when(
    (col('COR_SHELF') == 'VERMELHO')
    &(col('COR_AGING') == 'VERMELHO')
    , 'VERMELHO'
  ) \
)

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
    'ORDEM_COR_SHELF', when(col('COR_SHELF') == 'VERDE', 1) \
    .when(col('COR_SHELF') == 'AMARELO', 2) \
    .when(col('COR_SHELF') == 'LARANJA', 3) \
    .when(col('COR_SHELF') == 'VERMELHO', 4) \
    .otherwise(0)
  ).withColumn(
    'ORDEM_COR_AGING', when(col('COR_AGING') == 'VERDE', 1) \
    .when(col('COR_AGING') == 'AMARELO', 2) \
    .when(col('COR_AGING') == 'LARANJA', 3) \
    .when(col('COR_AGING') == 'VERMELHO', 4) \
    .otherwise(0)
  ).withColumn(
    'ORDEM_COR_INDICADOR_AS', when(col('COR_INDICADOR_AS') == 'VERDE', 1) \
    .when(col('COR_INDICADOR_AS') == 'AMARELO', 2) \
    .when(col('COR_INDICADOR_AS') == 'LARANJA', 3) \
    .when(col('COR_INDICADOR_AS') == 'VERMELHO', 4) \
    .otherwise(0)
  )

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
  'FILTRO_AGING', 
  when(col('AGING_DIAS') <= 30, '0 a 30') \
  .when(col('AGING_DIAS') <= 60, '30 a 60') \
  .when(col('AGING_DIAS') <= 90, '60 a 90') \
  .when(col('AGING_DIAS') <= 120, '90 a 120') \
  .when(col('AGING_DIAS') <= 180, '120 a 180') \
  .when(col('AGING_DIAS') <= 365, '180 a 365') \
  .when(col('AGING_DIAS') > 365, 'Acima de 365')
                                 ).orderBy('CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_LOCAL_ESTOQUE')

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
  'ORDEM_FILTRO_AGING', 
  when(col('AGING_DIAS') <= 30, 0) \
  .when(col('AGING_DIAS') <= 60, 1) \
  .when(col('AGING_DIAS') <= 90, 2) \
  .when(col('AGING_DIAS') <= 120, 3) \
  .when(col('AGING_DIAS') <= 180, 4) \
  .when(col('AGING_DIAS') <= 365, 5) \
  .when(col('AGING_DIAS') > 365, 6)
                                 ).orderBy('CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_LOCAL_ESTOQUE')

# COMMAND ----------

df_estoque_consolidado = df_estoque_consolidado.withColumn(
  'ORDEM_FLAG_SCORE', 
  when(col('FLAG_ITEM_CRITICO') == 'Score Elevado', 0) \
  .when(col('FLAG_ITEM_CRITICO') == 'Score Moderado', 1) \
  .when(col('FLAG_ITEM_CRITICO') == 'Não é critico', 2) \
                                 ).orderBy('CD_EMPRESA','CD_FILIAL','CD_ITEM','CD_LOCAL_ESTOQUE')

# COMMAND ----------

df_estoque_consolidado.write.format("bigquery").mode('overwrite').option("temporaryGcsBucket", "databricks-temp-bucket").option("table", 'seara-financeiro-nonprod.aging_estoque.estoque_fifo_com_aging').save()

# COMMAND ----------

# import pandas as pd

# df_pandas = df_estoque_consolidado.toPandas()

# COMMAND ----------

# df_pandas = df_pandas[
#   (df_pandas['DESTINO_FINAL_INDICADOR'].isin(['MERCADO EXTERNO', 'MERCADO INTERNO']))
#   &(df_pandas['FLAG_ITEM_CRITICO'] != 'Não é critico')
# ].sort_values(by=['SCORE'], ascending = False)

# COMMAND ----------

# df_pandas = df_pandas[[
#  'CD_EMPRESA',
# #  'CD_FILIAL',
# #  'CD_ITEM',
#  'FILTRO_FILIAL',
# #  'CD_LOCAL_ESTOQUE',
#  'FILTRO_LOCAL_ESTOQUE',
#  'FILTRO_ITEM',
#  'QT_DIAS_VALIDADE',
# #  'NM_GRUPO',
# #  'NM_SUBGRUPO',
# #  'NM_CLASSE',
#  'DS_DESTINO_FINAL',
#  'NM_NEGOCIO',
#  'NM_NEGOCIO_COMERCIAL',
#  'NM_TIPO_ACLIMATACAO_PRO',
# #  'MES',
#  'QT_PESO',
#  'SHELF',
#  'AGING_DIAS',
# #  'BIN_AGING_DIAS',
# #  'ORDEM_BIN',
# #  'DIRETORIA_ADJUNTA',
#  'VL_UNITARIO',
#  'VL_ITEM',
# #  'PERCENTIL_VL_UNITARIO',
# #  'PERCENTIL_VL_SALDO',
#  'SCORE',
#  'FLAG_ITEM_CRITICO',
#  'COR_SHELF',
# #  'DESTINO_FINAL_INDICADOR',
# #  'ACLIMATACAO_INDICADOR',
#  'COR_AGING',
#  'COR_INDICADOR_AS',
# #  'ORDEM_COR_SHELF',
# #  'ORDEM_COR_AGING',
# #  'ORDEM_COR_INDICADOR_AS',
#  'FILTRO_AGING'
# ]]

# COMMAND ----------

# df_pandas.to_excel('gs://mrp-estoque-ideal/estoques_fifo_com_aging.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Distribuição de aging

# COMMAND ----------

# df = df_estoque_consolidado.toPandas()

# COMMAND ----------

# df = df[
#   (~df['COR_SHELF'].isna())
#   &(~df['COR_AGING'].isna())
#   &(df['TABELA'] == 'FRIGORIFICADOS')
# ]

# COMMAND ----------

# df = df[df['MES'] == datetime.date(2021,10,1)]

# COMMAND ----------

# df_agg = df.groupby(['DESTINO_FINAL_INDICADOR', 'ACLIMATACAO_INDICADOR', 'COR_SHELF','COR_AGING','COR_INDICADOR_AS']).agg({'VL_ITEM': 'sum'}).reset_index().sort_values(by=[
#   'DESTINO_FINAL_INDICADOR','ACLIMATACAO_INDICADOR','COR_SHELF','COR_AGING',
# ])

# COMMAND ----------

# df_agg.to_excel('gs://mrp-estoque-ideal/distribuicao_cores_estoque.xlsx', index = False)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Consultas

# COMMAND ----------

# from google.cloud import bigquery as bq
# client = bq.Client()

# COMMAND ----------

# def gera_consulta_excel_estoque_fifo(cd_filial, cd_item, data_referencia):

#   query = f"""
#   SELECT 
#     his_saldo_estoque_fifo_dia.CD_EMPRESA,
#     his_saldo_estoque_fifo_dia.CD_FILIAL,
#     his_saldo_estoque_fifo_dia.CD_ITEM,
#     ITEM.NM_ITEM,
#     his_saldo_estoque_fifo_dia.CD_LOCAL_ESTOQUE,
#     local_estoque.DS_LOCAL_ESTOQUE,
#     his_saldo_estoque_fifo_dia.CD_SUBLOCAL_ESTOQUE,
#     PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_REFERENCIA) AS DT_REFERENCIA,
#     QT_VOLUME,
#     QT_PESO,
#     PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_FABRICACAO) AS DT_FABRICACAO,
#     PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_VALIDADE) AS DT_VALIDADE,
#     ROUND(100*SAFE_DIVIDE(DATE_DIFF(PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_REFERENCIA),PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_FABRICACAO), DAY), ITEM.QT_DIAS_VALIDADE),2) AS SHELF,
#     ROUND(DATE_DIFF(PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_REFERENCIA),PARSE_DATETIME('%d/%m/%Y %H:%M:%S', DT_FABRICACAO), DAY),2) AS AGING_DIAS,
#     CD_PALETE,
#     DS_END_PALLET
#   FROM 
#     `seara-analytics-prod.stg_erp_seara.his_saldo_estoque_fifo_dia` his_saldo_estoque_fifo_dia
#   LEFT JOIN 
#     `seara-analytics-prod.stg_erp_seara.item` ITEM
#   ON
#     ITEM.CD_ITEM = his_saldo_estoque_fifo_dia.CD_ITEM
#   LEFT JOIN 
#     `seara-analytics-prod.stg_erp_seara.local_estoque` local_estoque  
#   ON
#     local_estoque.cd_local_estoque = his_saldo_estoque_fifo_dia.CD_LOCAL_ESTOQUE
#   WHERE
#     his_saldo_estoque_fifo_dia.CD_FILIAL = {cd_filial}
#     AND his_saldo_estoque_fifo_dia.CD_ITEM = {cd_item}
#     AND PARSE_DATETIME('%d/%m/%Y %H:%M:%S',DT_REFERENCIA) = '{data_referencia}'
#   ORDER BY 
#     AGING_DIAS DESC
#   """

#   df_consulta = client.query(query).to_dataframe()
  
#   df_consulta.to_excel(f'gs://aging_estoque/consulta_filial_{cd_filial}_{cd_item}_{data_referencia}.xlsx', index = False)
  
#   return print('Concluído.')

# COMMAND ----------

# gera_consulta_excel_estoque_fifo(778, 981524, '2022-01-17')
